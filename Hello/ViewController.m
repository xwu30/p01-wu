//
//  ViewController.m
//  Hello
//
//  Created by Xian Wu on 1/30/16.
//  Copyright © 2016 Xian Wu. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *image1;

@end

@implementation ViewController
@synthesize label1,label2;

- (void)viewDidLoad {
    [super viewDidLoad];
    //UIImageView *backgroundImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"meihua.jpg"]];
    //[self.view addSubview:backgroundImage];
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"nian.jpeg"]]];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:self.view.bounds];
    //imageView.
    //CGRect rect = [[UIScreen mainScreen]bounds];
    //CGSize size = rect.size;
    //CGFloat width = size.width;
    //CGFloat height = size.height;
    //[self.view setFrame:rect];
    // Do any additional setup after loading the view, typically from a nib.
    
}

- (IBAction)button1:(id)sender {
    [UIView beginAnimations:@"xiaoguo" context:nil];
    [UIView setAnimationDuration:1.0f];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:self.view cache:YES];
    [UIView commitAnimations];
    //above animation was learnt from Baidu.tieba
    label1.text = @"Xian Wu";
    label2.text = @"Happy Chinese New Year";
//    label1.textColor = UIColor.redColor;
    label2.textColor = UIColor.redColor;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
