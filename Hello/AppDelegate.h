//
//  AppDelegate.h
//  Hello
//
//  Created by Xian Wu on 1/30/16.
//  Copyright © 2016 Xian Wu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

